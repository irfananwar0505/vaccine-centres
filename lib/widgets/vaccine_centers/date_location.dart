import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

import 'package:vaccine/models/district_name_and_id_model.dart';
import 'package:vaccine/themes/colors.dart';
import 'package:vaccine/themes/global_themes.dart';
import 'package:vaccine/utils/responsiveness.dart';
import 'package:vaccine/utils/shared_preferences.dart';

/// [SelectDateTime] is a widget where user can select a district and date to filter out vaccine center data
class SelectDateTime extends StatefulWidget {
  final Function refresh;
  final List<District> districts;
  final String prevDistrict;
  final String lastUpdated;
  const SelectDateTime(
      {this.refresh, this.districts, this.prevDistrict, this.lastUpdated});
  @override
  _SelectDateTimeState createState() => _SelectDateTimeState();
}

class _SelectDateTimeState extends State<SelectDateTime> {
  final DateFormat formatter = DateFormat('dd-MM-yyyy');
  District selectedDistrict;
  DateTime selectedDate = DateTime.now();
  String displayDate = '';

  void initState() {
    super.initState();
    if (widget.prevDistrict == null) {
      selectedDistrict = widget.districts[0];
    } else {
      int index = widget.districts.indexWhere(
          (element) => widget.prevDistrict == element.districtId.toString());
      selectedDistrict = widget.districts[index];
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day),
        lastDate: DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day + 20));

    if (picked != null && picked != selectedDate) {
      setState(() {
        // final String formatted = formatter.format(picked);
        selectedDate = picked;
      });
      displayDate = formatter.format(selectedDate);
      widget.refresh(displayDate, selectedDistrict.districtId.toString());
    }
  }

  void addCurrentDistrict({String id}) async {
    await SharedPreferencesUtils().addStringPrefs('district', id);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0.s),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.s),
              side: BorderSide(
                color: AppColors.white,
                width: 1.w,
              ),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 14.hs,
                vertical: 16.vs,
              ),
              child: Column(
                children: [
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              'District',
                              style: Theme.of(context).textTheme.boldHeadline5,
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: DropdownButton<District>(
                              value: selectedDistrict,
                              onChanged: (District newValue) {
                                addCurrentDistrict(
                                    id: newValue.districtId.toString());
                                setState(
                                  () {
                                    selectedDistrict = newValue;
                                    widget.refresh(displayDate,
                                        selectedDistrict.districtId.toString());
                                  },
                                );
                              },
                              items: widget.districts.map(
                                (District districts) {
                                  return DropdownMenuItem<District>(
                                    value: districts,
                                    child: Text(
                                      districts.districtName,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              'Date',
                              style: Theme.of(context).textTheme.boldHeadline5,
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: TextButton(
                              style: ButtonStyle(
                                elevation: MaterialStateProperty.all<double>(5),
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                  AppColors.blackSqueeze,
                                ),
                                foregroundColor:
                                    MaterialStateProperty.all<Color>(
                                  AppColors.bluemagenta,
                                ),
                              ),
                              onPressed: () => _selectDate(context),
                              child: Text(
                                formatter.format(selectedDate),
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Text(
            'Selected district will be saved as default*',
            style: Theme.of(context).textTheme.lightBodyText1,
          ),
          SizedBox(
            height: 8.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Last Updated - ${widget.lastUpdated}',
                style: Theme.of(context).textTheme.headline6withLightGreenColor,
              ),
              IconButton(
                splashColor: AppColors.gradientGreen,
                icon: Icon(Icons.refresh_sharp),
                onPressed: () => widget.refresh(formatter.format(selectedDate),
                    selectedDistrict.districtId.toString()),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
