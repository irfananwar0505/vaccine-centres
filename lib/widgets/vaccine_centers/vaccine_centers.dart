import 'package:flutter/material.dart';

import 'package:vaccine/models/covid_vaccine_center_model.dart';
import 'package:vaccine/themes/colors.dart';
import 'package:vaccine/utils/responsiveness.dart';
import 'package:vaccine/themes/global_themes.dart';
import 'package:vaccine/widgets/tooltip/custom_tool_tip.dart';

/// [VaccineCenters] is a widget which accepts all the details about vaccine centers
/// and displays them in a tabular format
class VaccineCenters extends StatelessWidget {
  final List<CovidVaccineCenter> vaccineCenters;
  final int totalSlotsAvailable;
  final String loading;
  const VaccineCenters({
    this.vaccineCenters,
    this.totalSlotsAvailable,
    this.loading,
  });
  @override
  build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 18.hs,
        right: 18.hs,
        bottom: 24.vs,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Row(
          //   children: [
          //     Expanded(flex: 2, child: Text("")),
          //     Expanded(
          //       flex: 1,
          //       child: Text(
          //         'Dose for age 45+',
          //         style: Theme.of(context).textTheme.subtitle2Disabled,
          //       ),
          //     ),
          //   ],
          // ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.hs, vertical: 8.vs),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: Text(
                    "Vaccine Centers",
                    style: Theme.of(context).textTheme.subtitle2Disabled,
                  ),
                ),
                SizedBox(
                  width: 15.w,
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Slots',
                        style: Theme.of(context).textTheme.subtitle2Disabled,
                      ),
                      CustomTooltip(
                        margin: EdgeInsets.only(left: 150.hs),
                        isIcon: true,
                        verticalOffset: 20.vs,
                        borderRadius: 20,
                        positionedCoordiante: 58,
                        padding: EdgeInsets.symmetric(
                          vertical: 14.vs,
                          horizontal: 16.hs,
                        ),
                        message:
                            'The slot counts shown below are including Dose 1 and Dose 2',
                      ),
                    ],
                  ),
                ),
                // Expanded(
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: [
                //       Text(
                //         '2nd',
                //         style: Theme.of(context).textTheme.subtitle2Disabled,
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
          vaccineCenters.length > 0
              ? Container(
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: vaccineCenters.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        color: AppColors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.s),
                          side: BorderSide(
                            color: AppColors.white,
                            width: 1.w,
                          ),
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.hs, vertical: 8.vs),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Text(
                                      vaccineCenters[index].blockName != ''
                                          ? '${vaccineCenters[index].blockName}, ${vaccineCenters[index].name}'
                                          : '${vaccineCenters[index].name}',
                                      style: vaccineCenters[index]
                                                  .sessions[0]
                                                  .availableCapacity >
                                              0
                                          ? Theme.of(context)
                                              .textTheme
                                              .slotAvailable
                                          : Theme.of(context)
                                              .textTheme
                                              .slotNotAvailable,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15.w,
                                  ),
                                  Expanded(
                                    child: Center(
                                      child: Text(
                                        '${vaccineCenters[index].sessions[0].availableCapacity}',
                                        style: vaccineCenters[index]
                                                    .sessions[0]
                                                    .availableCapacity >
                                                0
                                            ? vaccineCenters[index]
                                                        .sessions[0]
                                                        .availableCapacity <
                                                    10
                                                ? Theme.of(context)
                                                    .textTheme
                                                    .slotAvailable
                                                : Theme.of(context)
                                                    .textTheme
                                                    .slotAvailable
                                            : Theme.of(context)
                                                .textTheme
                                                .slotNotAvailable,
                                      ),
                                    ),
                                  ),
                                  // Expanded(
                                  //   child: Center(
                                  //     child: Text(
                                  //       '${vaccineCenters[index].sessions[0].availableCapacityDose2}',
                                  //       style: vaccineCenters[index]
                                  //                   .sessions[0]
                                  //                   .availableCapacity >
                                  //               0
                                  //           ? vaccineCenters[index]
                                  //                       .sessions[0]
                                  //                       .availableCapacity <
                                  //                   10
                                  //               ? Theme.of(context)
                                  //                   .textTheme
                                  //                   .slotAvailable
                                  //               : Theme.of(context)
                                  //                   .textTheme
                                  //                   .slotAvailable
                                  //           : Theme.of(context)
                                  //               .textTheme
                                  //               .slotNotAvailable,
                                  //     ),
                                  //   ),
                                  // ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              : Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 20.vs),
                    child: Text(
                      loading == ''
                          ? "No slots available for the selected date and district"
                          : loading,
                      style: Theme.of(context).textTheme.slotNotAvailable,
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
