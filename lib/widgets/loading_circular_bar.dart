import 'package:flutter/material.dart';

import 'package:vaccine/utils/responsiveness.dart';
import 'package:vaccine/themes/colors.dart';

/// Use [LoadingCircularBar] in the places of loader.
/// If you want to use for Page use like this
///  LoadingCircularBar();
/// If you want to use for Buttons use like this
/// LoadingCircularBar(
/// isButtonLoader = true;
/// );
///

class LoadingCircularBar extends StatelessWidget {
  final bool isButtonLoader;
  final double strokeWidth;

  LoadingCircularBar({
    this.isButtonLoader = false,
    this.strokeWidth,
  });
  @override
  Widget build(BuildContext context) {
    final Widget loader = CircularProgressIndicator(
      backgroundColor: AppColors.darkBlue,
      valueColor: AlwaysStoppedAnimation<Color>(
        AppColors.white,
      ),
      strokeWidth: strokeWidth ?? 4.0,
    );
    return isButtonLoader
        ? SizedBox(
            height: 20.d,
            width: 20.d,
            child: loader,
          )
        : loader;
  }
}
