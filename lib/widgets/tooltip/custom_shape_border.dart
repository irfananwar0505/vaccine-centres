import 'package:flutter/material.dart';

import 'package:vaccine/themes/colors.dart';
import 'package:vaccine/utils/responsiveness.dart';

class CustomShapeBorder extends ShapeBorder {
  /// Use [CustomShapeBorder] in the places where you want border in Custom Shapes.
  ///  ```dart
  ///  CustomShapeBorder({
  ///    borderRadius: the border radius of the container
  ///  });
  ///  ```
  final double borderRadius;
  //To position the arrow in the tooltip
  final bool isLeft;
  //The pass the values of xcoordinates
  final double positionedCoordiante;
  CustomShapeBorder({
    this.borderRadius,
    this.isLeft,
    this.positionedCoordiante,
  });
  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(0);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) => null;

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    return Path()
      ..moveTo(rect.left + borderRadius, rect.top)
      //if the arrow is to be positioned in left then isLeft should be passed true and the value will be added with the rect.left , in case of right isLeft should be false and the value will be subtracted with rect.right
      ..lineTo(
          isLeft
              ? rect.left + positionedCoordiante
              : rect.right - positionedCoordiante,
          rect.top)
      ..relativeLineTo(6.2, -6)
      ..relativeLineTo(6.2, 6)
      ..lineTo(rect.right - 18.6, rect.top)
      ..lineTo(rect.right - borderRadius, rect.top)
      ..arcToPoint(Offset(rect.right, rect.top + borderRadius),
          radius: Radius.circular(borderRadius))
      ..lineTo(rect.right, rect.bottom - borderRadius)
      ..arcToPoint(Offset(rect.right - borderRadius, rect.bottom),
          radius: Radius.circular(borderRadius))
      ..lineTo(rect.left + borderRadius, rect.bottom)
      ..arcToPoint(Offset(rect.left, rect.bottom - borderRadius),
          radius: Radius.circular(borderRadius))
      ..lineTo(rect.left, rect.top + borderRadius)
      ..arcToPoint(Offset(rect.left + borderRadius, rect.top),
          radius: Radius.circular(borderRadius));
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {
    Paint paint = Paint()
      ..color = AppColors.darkBlue
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.2.s;
    canvas.drawPath(getOuterPath(rect), paint);
  }

  @override
  ShapeBorder scale(double t) => this;
}
