import 'package:flutter/material.dart';

import 'package:vaccine/themes/colors.dart';
import 'package:vaccine/themes/global_themes.dart';
import 'package:vaccine/utils/responsiveness.dart';
import 'package:vaccine/widgets/tooltip/custom_shape_border.dart';

class CustomTooltip extends StatelessWidget {
  /// Use [CustomTooltip] in the places where you want Tooltip.
  ///  ```dart
  ///  CustomTooltip({
  ///    height: the height of the tooltip message container
  ///    margin: the empty space that surrounds the tooltip
  ///    padding: the amount of space by which to inset the tooltip's child
  ///    message: the message to be displayed in the tooltip
  ///  });
  ///  ```

  final double height;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final double verticalOffset;
  final bool preferBelow;
  final String message;
  final TextStyle textStyle;
  final Decoration decoration;
  final Color bgColor;
  final dynamic icon;
  final Color iconColor;
  final double iconSize;
  final double borderRadius;
  final bool isIcon;
  final Widget imageWidget;
  final bool isLeft;
  final double positionedCoordiante;

  CustomTooltip({
    this.height,
    this.margin,
    this.padding,
    this.verticalOffset = 20.0,
    this.preferBelow = true,
    this.message,
    this.textStyle,
    this.decoration,
    this.bgColor,
    this.iconSize = 24,
    this.icon = Icons.info_outline,
    this.iconColor,
    this.borderRadius = 10.0,
    this.isIcon = true,
    this.imageWidget,
    this.isLeft = false,
    this.positionedCoordiante = 31,
  });

  @override
  Widget build(BuildContext context) {
    GlobalKey _toolTipKey = GlobalKey(debugLabel: 'tooltip');
    return GestureDetector(
      onTap: () {
        final dynamic tooltip = _toolTipKey.currentState;
        tooltip.ensureTooltipVisible();
        Future.delayed(Duration(seconds: 5), () async {
          tooltip.deactivate();
        });
      },
      child: Tooltip(
        key: _toolTipKey,
        height: height,
        margin: margin ?? EdgeInsets.all(0.s),
        padding: padding ?? EdgeInsets.all(16.s),
        verticalOffset: verticalOffset,
        preferBelow: preferBelow,
        waitDuration: Duration(milliseconds: 0),
        showDuration: Duration(milliseconds: 5000),
        message: message,
        textStyle: textStyle ?? Theme.of(context).textTheme.secondarySubtitle2,
        decoration: ShapeDecoration(
          shape: CustomShapeBorder(
              borderRadius: borderRadius,
              isLeft: isLeft,
              positionedCoordiante: positionedCoordiante),
          color: bgColor ?? AppColors.white,
          shadows: [
            BoxShadow(
              color: AppColors.carbonBlack.withOpacity(0.1),
              blurRadius: 16.0,
              offset: Offset(0, 8),
            ),
          ],
        ),
        child: isIcon
            ? Icon(
                icon,
                size: iconSize,
                color: iconColor ?? AppColors.darkBlue,
              )
            : imageWidget,
      ),
    );
  }
}
