import 'package:flutter/material.dart';

import 'package:vaccine/containers/covid_vaccine_slots_container.dart';

class SearchForVaccineSlots extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: CovidVaccineSlotsContainer(),
    );
  }
}
