// To parse this JSON data, do
//
//     final districtNameAndId = districtNameAndIdFromJson(jsonString);

import 'dart:convert';

DistrictNameAndId districtNameAndIdFromJson(String str) => DistrictNameAndId.fromJson(json.decode(str));

String districtNameAndIdToJson(DistrictNameAndId data) => json.encode(data.toJson());

class DistrictNameAndId {
    DistrictNameAndId({
        this.districts,
        this.ttl,
    });

    List<District> districts;
    int ttl;

    factory DistrictNameAndId.fromJson(Map<String, dynamic> json) => DistrictNameAndId(
        districts: List<District>.from(json["districts"].map((x) => District.fromJson(x))),
        ttl: json["ttl"],
    );

    Map<String, dynamic> toJson() => {
        "districts": List<dynamic>.from(districts.map((x) => x.toJson())),
        "ttl": ttl,
    };
}

class District {
    District({
        this.districtId,
        this.districtName,
    });

    int districtId;
    String districtName;

    factory District.fromJson(Map<String, dynamic> json) => District(
        districtId: json["district_id"],
        districtName: json["district_name"],
    );

    Map<String, dynamic> toJson() => {
        "district_id": districtId,
        "district_name": districtName,
    };
}
