import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'package:vaccine/models/covid_vaccine_center_model.dart';
import 'package:vaccine/models/district_name_and_id_model.dart';

class CovidService {
  /// [getVaccineCenterData] gets the vaccine center details based on [date] and [distict] details passed
  Future<List<CovidVaccineCenter>> getVaccineCenterData({
    String date,
    String district,
  }) async {
    DateTime now = new DateTime.now();
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(now);
    String apiDate = date == null ? formatted : date;
    String districtId = district == null ? '301' : district;
    String cowinUrl =
        'https://cdn-api.co-vin.in/api/v2/appointment/sessions/calendarByDistrict?district_id=$districtId&date=$apiDate';
    var url = Uri.parse(cowinUrl);
    var response = await http.get(url, headers: {'Accept-Language': 'hi_IN'});
    List<CovidVaccineCenter> covidVaccineSlots = [];
    try {
      Map<String, dynamic> covidVaccineSlotList = jsonDecode(response.body);
      List slots = covidVaccineSlotList['centers'];

      slots.asMap().forEach((key, slots) {
        covidVaccineSlots.add(
          CovidVaccineCenter.fromJson(slots),
        );
      });
    } catch (e) {
      print(e);
    }
    return covidVaccineSlots;
  }

  /// [getDistrictNameAndId] gets the district name and district id
  Future<List<District>> getDistrictNameAndId() async {
    String districtAndId =
        'https://cdn-api.co-vin.in/api/v2/admin/location/districts/17';
    var url = Uri.parse(districtAndId);
    var response = await http.get(url, headers: {'Accept-Language': 'hi_IN'});
    List<District> districtNameAndId = [];
    try {
      Map<String, dynamic> districtNameAndIdMap = jsonDecode(response.body);
      List slots = districtNameAndIdMap['districts'];

      slots.asMap().forEach((key, slots) {
        districtNameAndId.add(
          District.fromJson(slots),
        );
      });
    } catch (e) {
      print(e);
    }
    return districtNameAndId;
  }
}
