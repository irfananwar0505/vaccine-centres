import 'package:flutter/material.dart';

import 'package:pigment/pigment.dart';

/// Define all the colors to be used in application in this file
/// To use - import this file and call required string by:
///```dart
///      AppColors.<name>
///```
///For Color Names refer: http://chir.ag/projects/name-that-color/#6195ED
class AppColors {
  AppColors._();
  static final Color white = Pigment.fromCSSColor(CSSColor.white);
  static final Color black = Pigment.fromString('#000000');
  static final Color disabled = Pigment.fromString('#7D7D7D');
  static final Color ghost = Pigment.fromString('#C9C9D2');
  static final Color gradientGreen = Pigment.fromString('#01A2A2');
  static const Color blackSqueeze = Color(0xFFE9F6F6);
  static final Color paleBlue = Pigment.fromString('#E9F0FC');
  static final Color carbonBlack = Pigment.fromString('#1F2134');
  static final Color carbonGrey = Pigment.fromString('#727384');
  static final Color darkBlue = Pigment.fromString('#4652FD');
  static final Color negativeRed = Pigment.fromString('#D04B4B');
  static final Color chateauGreen = Pigment.fromString('#3CB15B');
  static final Color gradientLightGreen = Pigment.fromString('#22A5A5');
  static const Color GOOGLE_TEXT_COLOR = Color.fromRGBO(0, 0, 0, 0.54);
  static final Color positiveGreen = Pigment.fromString('#2D8444');
  static final Color mediumOrange = Pigment.fromString('#FDA06D');
  static final Color bluemagenta = Pigment.fromString('#B597FF');
  static final Color lightGreen = Pigment.fromString('#698714');
}
