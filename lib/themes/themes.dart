import 'package:flutter/material.dart';

import 'package:vaccine/utils/responsiveness.dart';
import 'package:vaccine/themes/colors.dart';

class Themes {
  Themes._();

  // static final ThemeData lightTheme = _buildLightTheme();
  static final ThemeData baseTheme = ThemeData.light();

  static ThemeData buildLightTheme() {
    return ThemeData(
      fontFamily: 'Roboto',
      scaffoldBackgroundColor: AppColors.paleBlue,
      primaryColor: AppColors.carbonBlack,
      accentColor: AppColors.carbonGrey,
      appBarTheme: _buildAppBarTheme(),
      textTheme: _buildLightTextTheme(),
      errorColor: AppColors.negativeRed,
    );
  }

  static AppBarTheme _buildAppBarTheme() {
    return AppBarTheme(elevation: 0);
  }

  static TextTheme _buildLightTextTheme() {
    return baseTheme.textTheme
        .copyWith(
          headline1: baseTheme.textTheme.headline1.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 32.f,
            fontWeight: FontWeight.w500,
          ),
          headline2: baseTheme.textTheme.headline2.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 24.f,
            fontWeight: FontWeight.w500,
          ),
          headline3: baseTheme.textTheme.headline3.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 24.f,
            fontWeight: FontWeight.w400,
          ),
          headline4: baseTheme.textTheme.headline4.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 20.f,
            fontWeight: FontWeight.w500,
          ),
          headline5: baseTheme.textTheme.headline5.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 16.f,
            fontWeight: FontWeight.w500,
          ),
          headline6: baseTheme.textTheme.headline6.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 14.f,
            fontWeight: FontWeight.w500,
          ),
          subtitle1: baseTheme.textTheme.subtitle1.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 16.f,
            fontWeight: FontWeight.w400,
          ),
          subtitle2: baseTheme.textTheme.subtitle2.copyWith(
            color: AppColors.carbonBlack,
            fontSize: 14.f,
            fontWeight: FontWeight.w400,
          ),
          bodyText1: baseTheme.textTheme.bodyText1.copyWith(
            color: AppColors.black,
            fontSize: 12.f,
            fontWeight: FontWeight.w500,
          ),
          bodyText2: baseTheme.textTheme.bodyText2.copyWith(
            color: AppColors.white,
            fontSize: 12.f,
            fontWeight: FontWeight.w400,
          ),
          button: baseTheme.textTheme.button.copyWith(
            color: AppColors.white,
            fontSize: 16.f,
            fontWeight: FontWeight.w500,
          ),
        )
        .apply(
          fontFamily: 'Roboto',
        );
  }
}
