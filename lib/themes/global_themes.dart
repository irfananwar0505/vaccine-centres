import 'package:flutter/material.dart';

import 'package:vaccine/themes/colors.dart';

extension ThemeTextExtension on TextTheme {
  TextStyle get slotAvailable =>
      headline6.copyWith(color: AppColors.positiveGreen);
  TextStyle get slotNotAvailable =>
      headline6.copyWith(color: AppColors.negativeRed);
  TextStyle get slotAlmostFilled =>
      headline6.copyWith(color: AppColors.mediumOrange);
  TextStyle get secondarySubtitle2 =>
      subtitle2.copyWith(color: AppColors.carbonGrey);
  TextStyle get subtitle2Disabled =>
      subtitle2.copyWith(color: AppColors.disabled);
  TextStyle get headline6withLightGreenColor =>
      headline6.copyWith(color: AppColors.lightGreen);
  TextStyle get lightBodyText1 =>
      bodyText1.copyWith(fontWeight: FontWeight.w400);
  TextStyle get boldHeadline5 =>
      headline5.copyWith(fontWeight: FontWeight.w700);
}
