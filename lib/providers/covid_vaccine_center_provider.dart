import 'package:flutter/material.dart';

import 'package:vaccine/models/covid_vaccine_center_model.dart';
import 'package:vaccine/models/district_name_and_id_model.dart';

/// [CovidVaccineCenterProvider] is the global store for Vaccine Center details.
class CovidVaccineCenterProvider extends ChangeNotifier {
  List<District> districtDetails = [];
  List<CovidVaccineCenter> covidVaccineCenter;

  /// [setClickedGroupId] sets the CovidVaccineCenter details
  set setVaccineCentersData(List<CovidVaccineCenter> vaccineCenters) {
    covidVaccineCenter = vaccineCenters;
    notifyListeners();
  }

  /// [setClickedGroupId] sets the CovidVaccineCenter details
  set setdistrictDetails(List<District> districts) {
    districtDetails = [];
    districtDetails = districts;
    notifyListeners();
  }
}
