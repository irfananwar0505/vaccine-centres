import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:vaccine/services/services.dart';
import 'package:vaccine/utils/responsiveness.dart';
import 'package:vaccine/utils/shared_preferences.dart';
import 'package:vaccine/models/pref_status_model.dart';
import 'package:vaccine/widgets/loading_circular_bar.dart';
import 'package:vaccine/models/district_name_and_id_model.dart';
import 'package:vaccine/models/covid_vaccine_center_model.dart';
import 'package:vaccine/providers/covid_vaccine_center_provider.dart';
import 'package:vaccine/widgets/vaccine_centers/date_location.dart';
import 'package:vaccine/widgets/vaccine_centers/vaccine_centers.dart';

/// [CovidVaccineSlotsContainer] is the container which fetches the data related to vaccine centers
/// and pass them to [VaccineCenters]
/// It also holds the [SelectDateTime] widget
class CovidVaccineSlotsContainer extends StatefulWidget {
  @override
  _CovidVaccineSlotsContainerState createState() =>
      _CovidVaccineSlotsContainerState();
}

class _CovidVaccineSlotsContainerState
    extends State<CovidVaccineSlotsContainer> {
  CovidVaccineCenterProvider _covidVaccineCenterProvider;
  List<CovidVaccineCenter> centersData = [];
  List<District> districts = [];
  int totalSlotsAvailable = 0;
  String loading = '';
  DateFormat dateFormatter;
  DateFormat timeFormatter;
  String lastUpdated = '';
  PrefStatus selectedDistrict;
  @override
  void initState() {
    super.initState();
    _covidVaccineCenterProvider =
        Provider.of<CovidVaccineCenterProvider>(context, listen: false);
    dateFormatter = DateFormat('dd-MM-yyyy');
    timeFormatter = DateFormat('HH:mm:ss');
    SchedulerBinding.instance.addPostFrameCallback(
      (_) async {
        PrefStatus district =
            await SharedPreferencesUtils().getStringPrefs('district');
        setState(() {
          selectedDistrict = district;
        });
        getVaccineCentersDetails(
          dateFormatter.format(DateTime.now()),
          selectedDistrict.value,
        );
      },
    );
  }

  getVaccineCentersDetails(String date, [String district]) async {
    setState(() {
      centersData = [];
      loading = 'loading data, please wait!';
    });
    if (_covidVaccineCenterProvider.districtDetails.isEmpty) {
      _covidVaccineCenterProvider.setdistrictDetails =
          await CovidService().getDistrictNameAndId();
      setState(() {
        districts = _covidVaccineCenterProvider.districtDetails;
      });
    }
    _covidVaccineCenterProvider.setVaccineCentersData = await CovidService()
        .getVaccineCenterData(date: date, district: district);
    setState(() {
      centersData = _covidVaccineCenterProvider.covidVaccineCenter;
      loading = '';
      lastUpdated = timeFormatter.format(
        DateTime.now(),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CovidVaccineCenterProvider>(
      builder: (BuildContext context, covidVaccineCenterProvider, _) {
        if (covidVaccineCenterProvider.covidVaccineCenter == null) {
          return Center(
            heightFactor: 20.h,
            child: LoadingCircularBar(),
          );
        } else {
          covidVaccineCenterProvider.covidVaccineCenter.sort(
            (currentGroupMemberInfo, nextGroupMemberInfo) {
              int cmp = nextGroupMemberInfo.sessions[0].availableCapacity
                  .compareTo(
                      currentGroupMemberInfo.sessions[0].availableCapacity);
              if (cmp != 0) return cmp;
              return currentGroupMemberInfo.blockName
                  .compareTo(nextGroupMemberInfo.blockName);
            },
          );
          covidVaccineCenterProvider.covidVaccineCenter.map((center) {
            totalSlotsAvailable =
                totalSlotsAvailable + center.sessions[0].availableCapacity;
          }).toList();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SelectDateTime(
                refresh: getVaccineCentersDetails,
                districts: districts,
                prevDistrict: selectedDistrict?.value,
                lastUpdated: lastUpdated,
              ),
              Container(
                child: VaccineCenters(
                  vaccineCenters: centersData ?? [],
                  totalSlotsAvailable: totalSlotsAvailable,
                  loading: loading,
                ),
              ),
            ],
          );
        }
      },
    );
  }
}
