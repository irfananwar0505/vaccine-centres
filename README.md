# vaccine

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## Code Structure
Screens - Holds the screen level files (should not include any service calls or buisness logic)

Containers - Holds the widget which calls the service files and do the buisness logic required with the data

Services - Does API calls and returns data in the required format to Containers

Model - Data Model file

Widgets - Which accepts data and displays to the user